# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Attendee


SUBJECT = "DebConf20 Registration: T-Shirt Shipping"

TEMPLATE = """\
Dear {{ name }},

Just because we're all stuck at home doesn't mean we can't have shiny new
DebConf 20 t-shirts.

We are planning on printing some DebConf 20 t-shirts.  The logistics industry
is still strongly affected by Covid 19, but we're going to attempt having them
shipped directly to attendees.

If you would like a t-shirt, you will need to provide us with a shipping
address including recipient name, as well as a phone number we can provide to
the shipping company for notifications.  That doesn't need to be your home
address, but it does need to be an address where the parcel will reach you.
You can do so by editing your registration on the DebConf20 website:
<https://debconf20.debconf.org/register/>

Fill in the "Shipping Address" field on the "Personal Information" page.
The deadline to submit your address is 23:59 UTC on Sunday the 26th of July.

We only have a limited budget, and we may not be able to deliver to every
country in time, but we'll do our best.

Extra shirts will be available for purchase; please contact registration if
you'd like to order.

We're planning to ship from the EU¹.  Depending on where you live, you may have
to pay customs duties.

Looking forward to seeing you online

The DebConf20 T-shirt team

1: Except for some locally organised printing efforts, primarily in Brazil.
   https://wiki.debian.org/DebConf/20/Tshirts#Regional_printing_and_shipping
"""

class Command(BaseCommand):
    help = 'Send tshirt address emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        ctx = {
            'name': name,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
