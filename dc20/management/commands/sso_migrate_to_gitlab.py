from getpass import getpass

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.utils.http import urlencode

import requests

from wafer.kv.models import KeyValue


class Command(BaseCommand):
    help = 'Migrate Debian SSO to GitLab'

    def handle(self, *args, **options):
        token = getpass('GitLab Personal Access Token: ')
        group = Group.objects.get_by_natural_key('Registration')

        for kv in KeyValue.objects.filter(
                group=group, key='debian_sso_email', userprofile__isnull=False):
            username = kv.value.split('@', 1)[0]
            r = requests.get(
                'https://salsa.debian.org/api/v4/users?' + urlencode({
                    'username': username,
                }),
                headers={'Authorization': 'Bearer {}'.format(token),
            })
            if r.status_code != 200:
                self.stderr.write(
                    'Failed to look up {} in Salsa\n'.format(username))
                continue
            users = r.json()
            if len(users) != 1:
                self.stderr.write(
                    'Failed to look up {} in Salsa\n'.format(username))
                continue
            kv.key = 'gitlab_sso_account_id'
            kv.value = users[0]['id']
            kv.save()
            self.stdout.write('Converted {} to Salsa SSO\n'.format(username))
