---
name: Become a Sponsor
---
# Become a Sponsor

Thank you for your interest in DebConf, the annual conference of the
Debian Project!

This year the conference had to move online due to the Corona virus
making travel too inconvenient or even impossible for some of our
community members.

We offer package deals for the Silver, Gold and Platinum sponsors
that have pre-committed, please contact the [fundraising team](mailto:sponsors@debconf.org)
if you have not received our offer via email already.

If you would like to sponsor DebConf 20 Online, we are very happy to offer the following levels:

|                                              | supporter      | bronze  |  silver  |   gold   | platinum |
|----------------------------------------------|----------------|---------|----------|----------|----------|
| Contribution in USD                          |   up to $500   |   $500  |  $1,500  |   $3,000 |  $6,000  |
| Logo on sponsor webpage                      | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> |
| Logo on video streams and recordings         |           | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> | <i class="fa fa-check" aria-hidden="true"></i> |
| Mention in press releases                    |           |        |        |         | <i class="fa fa-check" aria-hidden="true"></i> |

As this conference is Online, the main perk we can offer is the presence on our video streams and recordings.    
The logos will be featured in sizes relative to the sponsorship level. Similarly to the different logo sizes on our
[sponsor web page](/sponsors/).

If you have any questions please do not hesitate to reach out to
[sponsors@debconf.org](mailto:sponsors@debconf.org).
